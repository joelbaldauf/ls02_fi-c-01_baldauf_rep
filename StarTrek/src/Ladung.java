/**
 * Eine Klasse, die Ladungen eines Raumschiffs darstellt
 *
 * @author "Joel Baldauf"
 * @version 0.2
 *
 */
public class Ladung {
    private String bezeichnung;
    private int menge;

    public Ladung() {

    }

    /**
     * parametisierter Konstruktor zum Erzeugen einer Ladung
     * @param bezeichnung Bezeichnung für die Ladung
     * @param menge Menge der Ladung
     */
    public Ladung(String bezeichnung, int menge) {
        this.setBezeichnung(bezeichnung);
        this.setMenge(menge);
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public int getMenge() {
        return menge;
    }

    public void setMenge(int menge) {
        this.menge = Math.max(menge, 0);
    }


}
