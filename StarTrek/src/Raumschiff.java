import java.util.ArrayList;
import java.util.Random;

/**
 * Eine Klasse, die ein Raumschiff für das Spiel "Raumschiffe" darstellt
 *
 * @author "Joel Baldauf"
 * @version 0.2
 *
 */
public class Raumschiff {

    private int photonentorpedoAnzahl;
    private int energieversorgungInProzent;
    private int schildeInProzent;
    private int huelleInProzent;
    private int lebenserhaltungssystemeInProzent;
    private int androidenAnzahl;
    private String schiffsname;
    private static final ArrayList<String> broadcastKommunikator = new ArrayList<>();
    private final ArrayList<Ladung> ladungsverzeichnis;


    public Raumschiff() {
        this.ladungsverzeichnis = new ArrayList<>();
    }

    /**
     * voll parametisierter Konstruktor zum Erzeugen eines Raumschiffs.
     *
     * @param photonentorpedoAnzahl Anzahl der in die Torpedoröhre geladenen Photonentorpedos
     * @param energieversorgungInProzent Zustand der Energieversorgung des Raumschiffs in Prozent
     * @param schildeInProzent  Zustand der Schilde des Raumschiffs in Prozent
     * @param huelleInProzent Zustand der Hülle des Raumschiffs in Prozent
     * @param lebenserhaltungssystemeInProzent Zustand der Lebenserhaltungssysteme des Raumschiffs in Prozent
     * @param androidenAnzahl Anzahl der für Reparaturen verfügbaren Reparaturandroiden des Schiffs
     * @param schiffsname Name des Raumschiffs
     */
    public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
        this.setPhotonentorpedoAnzahl(photonentorpedoAnzahl);
        this.setEnergieversorgungInProzent(energieversorgungInProzent);
        this.setSchildeInProzent(schildeInProzent);
        this.setHuelleInProzent(huelleInProzent);
        this.setLebenserhaltungssystemeInProzent(lebenserhaltungssystemeInProzent);
        this.setAndroidenAnzahl(androidenAnzahl);
        this.setSchiffsname(schiffsname);
        this.ladungsverzeichnis = new ArrayList<>();
    }

    public int getPhotonentorpedoAnzahl() {
        return photonentorpedoAnzahl;
    }

    public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
    }

    public int getEnergieversorgungInProzent() {
        return energieversorgungInProzent;
    }

    public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
        this.energieversorgungInProzent = energieversorgungInProzent;
    }

    public int getSchildeInProzent() {
        return schildeInProzent;
    }

    public void setSchildeInProzent(int schildeInProzent) {
        this.schildeInProzent = schildeInProzent;
    }

    public int getHuelleInProzent() {
        return huelleInProzent;
    }

    public void setHuelleInProzent(int huelleInProzent) {
        this.huelleInProzent = huelleInProzent;
    }

    public int getLebenserhaltungssystemeInProzent() {
        return lebenserhaltungssystemeInProzent;
    }

    public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
    }

    public int getAndroidenAnzahl() {
        return androidenAnzahl;
    }

    public void setAndroidenAnzahl(int androidenAnzahl) {
        this.androidenAnzahl = androidenAnzahl;
    }

    public String getSchiffsname() {
        return schiffsname;
    }

    public void setSchiffsname(String schiffsname) {
        this.schiffsname = schiffsname;
    }


    /**
     * Methode, die Ladung zum Ladungsverzeichnis hinzufügt, diese also dem Raumschiff zuordnet
     * @param neueLadung zum Ladungsverzeichnis hinzuzufügende Ladung
     */
    public void addLadung(Ladung neueLadung) {
        this.ladungsverzeichnis.add(neueLadung);
    }

    /**
     * Methode zum Abschießen eines Photonentorpedos
     * Gibt es keine Torpedos, so wird als Nachricht an Alle -=*Click*=- ausgegeben.
     * Ansonsten wird die Torpedoanzahl um eins reduziert und die Nachricht an Alle Photonentorpedo abgeschossen gesendet. Außerdem wird die Methode Treffer aufgerufen.
     * @param r Das Raumschiff, welches vom Torpedo getroffen werden soll
     */
    public void photonentorpedoSchiessen(Raumschiff r) {
        if (this.getPhotonentorpedoAnzahl() == 0) {
            nachrichtAnAlle("-=*Click*=-");
        } else {
            this.setPhotonentorpedoAnzahl(this.getPhotonentorpedoAnzahl() - 1);
            nachrichtAnAlle("Photonentorpedo abgeschossen");
            treffer(r);
        }
    }

    /**
     * Methode zum Abschießen einer Phaserkanone
     * Ist die Energieversorgung kleiner als 50%, so wird als Nachricht an Alle -=*Click*=- ausgegeben.
     * Ansonsten wird die Energieversorgung um 50% reduziert und die Nachricht an Alle “Phaserkanone abgeschossen” gesendet. Außerdem wird die Methode Treffer aufgerufen.
     * @param r Das Raumschiff, welches von der Phaserkanone getroffen werden soll
     */
    public void phaserkanoneSchiessen(Raumschiff r) {
        if (this.getEnergieversorgungInProzent() < 50) {
            nachrichtAnAlle("-=*Click*=-");
        } else {
            this.setEnergieversorgungInProzent(this.getEnergieversorgungInProzent() - 50);
            nachrichtAnAlle("Phaserkanone abgeschossen");
            treffer(r);
        }
    }

    /**
     * Methode, die aufgerufen wird, wenn ein Raumschiff getroffen wurde.
     * Es wird eine entsprechende Nachricht in der Konsole ausgegeben.
     * Die Schilde werden um 50% geschwächt
     * Sollte anschließend die Schilde vollständig zerstört worden sein, so wird der Zustand der Hülle und der Energieversorgung jeweils um 50% abgebaut.
     * Sollte danach der Zustand der Hülle auf 0% absinken, so sind die Lebenserhaltungssysteme vollständig zerstört und es wird eine Nachricht an Alle ausgegeben, dass die Lebenserhaltungssysteme vernichtet worden sind.
     * @param r @param r "Das Raumschiff, welches getroffen wurde"
     */
    private void treffer(Raumschiff r) {
        System.out.println(r.getSchiffsname() + " wurde getroffen!\n");

        //wenn Schilde vollständig zerstört
        if (r.getSchildeInProzent() < 1) {

            if (r.getHuelleInProzent() < 51) {
                r.setLebenserhaltungssystemeInProzent(0);
                nachrichtAnAlle("Lebenserhaltungssysteme von Raumschiff " + r.schiffsname + " vernichtet");
            }
                //Hülle um 50 Prozent abbauen
                r.setHuelleInProzent(r.getHuelleInProzent() - 50);
                //Energieversorgung um 50 Prozent abbauen
                r.setEnergieversorgungInProzent(r.getEnergieversorgungInProzent() - 50);
        } else {
            //Schilde um 50 Prozent schwächen
            r.setSchildeInProzent(r.getSchildeInProzent() - 50);
        }
    }


    /**
     * Methode, die eine Nachricht an alle in den BroadcastKommunikator einfügt
     * Die gesammelten Nachrichten können später über die Methode {@link #eintraegeLogbuchZurueckgeben()} zurückgegeben werden.
     * @param message In den BroadcastKommunikator einzutragende Nachricht
     */
    public void nachrichtAnAlle(String message) {
        broadcastKommunikator.add(this.schiffsname + ": " + message);
    }

    /**
     * @return Liste der Nachrichten, die an alle gesendet wurden
     */
    public ArrayList<String> eintraegeLogbuchZurueckgeben() {
        return broadcastKommunikator;
    }

    /**
     * Methode, um Torpedos von den Ladungen in die Torpedoröhre zu laden.
     * @param anzahlTorpedos Anzahl der zu ladenen Torpedos. Wenn größer angeben als verfügbar, so werden alle verfügbaren genutzt.
     */
    public void photonentorpedosLaden(int anzahlTorpedos) {
        if (this.ladungPhotonentorpedosAnzahl() == 0) {
            System.out.println("Keine Photonentorpedos gefunden!");
            nachrichtAnAlle("=*Click*=-");
        } else {
            //wenn Photonentorpedos mehr als in Ladung verfügbar, alle verfügbaren laden
            if (anzahlTorpedos > this.ladungPhotonentorpedosAnzahl())
                anzahlTorpedos = this.ladungPhotonentorpedosAnzahl();

            this.setPhotonentorpedoAnzahl(this.getPhotonentorpedoAnzahl() + anzahlTorpedos);
            ladungPhotonentorpedoVermindern(anzahlTorpedos);
            ladungsverzeichnisAufraeumen();
            System.out.println(anzahlTorpedos + " Photonentorpedo(s) eingesetzt\n");
        }
    }

    /**
     * Methode, um Reparaturen am Raumschiff durchzuführen und so dessen Zustand zu verbessern
     * @param schutzschilde true repariert die Schutzschilde, false repariert sie nicht
     * @param energieversorgung true repariert die Energieversorgung, false repariert sie nicht
     * @param schiffshuelle true repariert die Schiffshülle, false repariert sie nicht
     * @param anzahlDroiden Anzahl der für die Reparatur einzusetzenden Androiden. Wenn mehr als verfügbar angegeben, werden alle verfügbaren genutzt.
     */
    public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) {
        Random random = new Random();
        int zufallszahl = random.nextInt(100);
        int anzahlTrue = 0;

        if (schutzschilde) {
            anzahlTrue += 1;
        } if (energieversorgung) {
            anzahlTrue += 1;
        } if (schiffshuelle) {
            anzahlTrue += 1;
        }

        int reparaturErg = (zufallszahl * anzahlDroiden) / anzahlTrue;

        if (schutzschilde) {
            this.setSchildeInProzent(this.getSchildeInProzent() + reparaturErg);
        } if (energieversorgung) {
            this.setEnergieversorgungInProzent(this.getEnergieversorgungInProzent() + reparaturErg);
        } if (schiffshuelle) {
            this.setHuelleInProzent(this.getHuelleInProzent() + reparaturErg);
        }
    }

    /**
     * Gibt den aktuellen Zustand (also alle Attribute) des Raumschiffs auf der Konsole aus
     */
    public void zustandRaumschiff() {
        System.out.println("Schiffsname: " + this.schiffsname);
        System.out.println("photonentorpedoAnzahl: " + this.photonentorpedoAnzahl);
        System.out.println("energieversorgungInProzent: " + this.energieversorgungInProzent);
        System.out.println("schildeInProzent: " + this.schildeInProzent);
        System.out.println("huelleInProzent: " + this.huelleInProzent);
        System.out.println("lebenserhaltungssystemeInProzent: " + this.lebenserhaltungssystemeInProzent);
        System.out.println("androidenAnzahl: " + this.androidenAnzahl);
        System.out.println();
    }

    /**
     * Gibt das Ladungsverzeichnis, also alle dem Raumschiff zugeordneten Ladungen auf der Konsole aus.
     */
    public void ladungsverzeichnisAusgeben() {
        System.out.println("Ladungen des Raumschiffs " + this.schiffsname);
        System.out.println("============================================");
        for (Ladung ladung : this.ladungsverzeichnis) {
            System.out.println("Ladungsbezeichnung: " + ladung.getBezeichnung());
            System.out.println("Menge: " + ladung.getMenge());
            System.out.println("============================================");
        }
        System.out.println();
    }

    /**
     * Methode, die alle Ladungsobjekte mit einer Menge gleich 0 aus dem Ladungsverzeichnis entfernt
     */
    public void ladungsverzeichnisAufraeumen() {
        ladungsverzeichnis.removeIf(ladung -> ladung.getMenge() == 0);
    }

    /**
     * zusätzlich zur Aufgabenstellung implementierte Methode, die alle verfügbaren Photonentorpedos im Ladungsverzeichnis zählt
     * (auch für den Fall wenn bspw. mehrere Einzelladungen verfügbar sind)
     * @return Anzahl aktuell insgesamt verfügbarer Photonentorpedos im Ladungsverzeichnis
     */
    public int ladungPhotonentorpedosAnzahl() {
        int anzahl = 0;
        for (Ladung ladung : this.ladungsverzeichnis) {
            if (ladung.getBezeichnung().equalsIgnoreCase("photonentorpedo")) anzahl += ladung.getMenge();
        }
        return anzahl;
    }


    /**
     * zusätzlich zur Aufgabenstellung implementierte Methode, die in die Torpedoröhre geladene Torpedos im Gegenzug aus den Ladungsobjekten entfernt bzw. diesen abzieht
     * wird von {@link #photonentorpedosLaden(int)} aufgerufen
     * @param anzahl wie viele Photonentorpedos aus dem Ladungsverzeichnis entfernt werden sollen
     */
    public void ladungPhotonentorpedoVermindern(int anzahl) {
        int tempToRemove;
        for (Ladung ladung : this.ladungsverzeichnis) {
            if (ladung.getBezeichnung().equalsIgnoreCase("photonentorpedo") && anzahl > 0) {
                if (ladung.getMenge() < anzahl) {
                    tempToRemove = ladung.getMenge();
                    anzahl -= tempToRemove;
                } else {
                    tempToRemove = anzahl;
                    anzahl = 0;
                }
                ladung.setMenge(ladung.getMenge() - tempToRemove);
            }
        }
    }
}
