/**
 * Testklasse zum Überprüfen der Funktionalität des Raumschiffs sowie der Ladungen
 *
 * @author "Joel Baldauf"
 * @version 0.2
 *
 */
public class Test {
    /**
     * Hauptprogramm.
     *
     * @param args Kommandozeilenparameter
     */
    public static void main(String[] args) {
        Raumschiff klingonen = new Raumschiff(
                1,
                100,
                100,
                100,
                100,
                2,
                "IKS Hegh'ta");

        Raumschiff romulaner = new Raumschiff(
                2,
                100,
                100,
                100,
                100,
                2,
                "IRW Khazara");

        Raumschiff vulkanier = new Raumschiff(
                0,
                80,
                80,
                50,
                100,
                5,
                "Ni'Var");


        Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
        Ladung l2 = new Ladung("Borg-Schrott", 5);
        Ladung l3 = new Ladung("Rote Materie", 2);
        Ladung l4 = new Ladung("Forschungssonde", 35);
        Ladung l5 = new Ladung("Bat'leth Klingonen Schwert", 200);
        Ladung l6 = new Ladung("Plasma-Waffe", 50);
        Ladung l7 = new Ladung("Photonentorpedo", 3);

        klingonen.addLadung(l1);
        klingonen.addLadung(l5);
        romulaner.addLadung(l2);
        romulaner.addLadung(l3);
        romulaner.addLadung(l6);
        vulkanier.addLadung(l4);
        vulkanier.addLadung(l7);

        klingonen.photonentorpedoSchiessen(romulaner);
        romulaner.phaserkanoneSchiessen(klingonen);
        vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
        klingonen.zustandRaumschiff();
        klingonen.ladungsverzeichnisAusgeben();
        vulkanier.reparaturDurchfuehren(true, true, true, vulkanier.getAndroidenAnzahl());
        vulkanier.photonentorpedosLaden(vulkanier.ladungPhotonentorpedosAnzahl());

        for (int i = 0; i<2; i++) klingonen.photonentorpedoSchiessen(romulaner);

        klingonen.zustandRaumschiff();
        klingonen.ladungsverzeichnisAusgeben();

        romulaner.zustandRaumschiff();
        romulaner.ladungsverzeichnisAusgeben();

        vulkanier.zustandRaumschiff();
        vulkanier.ladungsverzeichnisAusgeben();

        //erweiternd zur Aufgabe werden hier die Logbucheinträge kontrolliert
        System.out.println(vulkanier.eintraegeLogbuchZurueckgeben());
    }
}
