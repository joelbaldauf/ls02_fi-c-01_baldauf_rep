package Angestellter;

public class TestAngestellter {

	public static void main(String[] args) {
		  
		//Erzeugen der Objekte
		Angestellter ang1 = new Angestellter ( "Manfred", "Meier", 4500 );
	    Angestellter ang2 = new Angestellter ( "Petersen", "Peter", 6000 );
	    Angestellter ang3 = new Angestellter ( "Baldauf", "Joel", 10000);
	    
	    /* 7. Erzeugen Sie ein zusaetzliches Objekt ang3 und geben Sie es auch auf der Konsole aus, 
	     * die Attributwerte denken Sie sich aus.
	     */  
	      
	      
	      
	    /*8. Erzeugen Sie zwei zusaetzliche Objekte ang4 und ang5
	     * mit dem Konstruktor, der den Namen und Vornamen initialisiert,
	     * die Attributwerte denken Sie sich aus.
	     */
	    
	    Angestellter ang4 = new Angestellter ("Mueller", "Max");
	    Angestellter ang5 = new Angestellter ("Meier", "Kevin");
	    
	    //Setzen der Attribute
	    /* 9. Fuegen Sie ang4 und ang5 jeweils ein Gehalt hinzu, 
	     * die Attributwerte denken Sie sich aus.
	     * Geben Sie ang4 und ang5 auch auf dem Bildschirm aus.
	     */

	    ang4.setGehalt(5000);
	    ang5.setGehalt(6500);
	    
        //Bildschirmausgabe
	    printAngestellterDetails(ang1);
	    printAngestellterDetails(ang2);
	    printAngestellterDetails(ang3);
	    printAngestellterDetails(ang4);
	    printAngestellterDetails(ang5);

	}

	
	public static void printAngestellterDetails(Angestellter ang) {
		 System.out.println("\nName: " + ang.getName());
		 System.out.println("Vorname: " + ang.getVorname());
		 System.out.println("Gehalt: " + ang.getGehalt() + " Euro");
		 System.out.println("Vollname: " + ang.vollname());
	}
}
