package Angestellter;

public class Angestellter {

	 private String name;
	 private String vorname;
	 private double gehalt;
	 
	 //3. Fuegen Sie in der Klasse 'Angestellter' das Attribut 'vorname' hinzu und implementieren Sie die entsprechenden get- und set-Methoden.
	 //4. Implementieren Sie einen Konstruktor, der alle Attribute initialisiert.
	 //5. Implementieren Sie einen Konstruktor, der den Namen und den Vornamen initialisiert.

	 public Angestellter (String name, String vorname, double gehalt) {
		 this.name = name;
		 this.vorname = vorname;
		 this.gehalt = gehalt;
	 }
	 
	 public Angestellter (String name, String vorname) {
		 this.name = name;
		 this.vorname = vorname;
	 }
	 
	 public void setVorname(String vorname) {
		    this.vorname = vorname;
		 } 
	 
	 public String getVorname() {
		    return this.vorname;
		 }
	 
	 public void setName(String name) {
	    this.name = name;
	 }
	 
	 public String getName() {
	    return this.name;
	 }

	 public void setGehalt(double gehalt) {
		 if (gehalt > 0) {
			 this.gehalt = gehalt;
		 }
	   //1. Implementieren Sie die entsprechende set-Methoden. 
	   //Ber�cksichtigen Sie, dass das Gehalt nicht negativ sein darf.
	 }
 
	 public double getGehalt() {
	   // 2. Implementieren Sie die entsprechende get-Methoden.
		 return this.gehalt;
	 }
	 
	 public String vollname() {
		 return this.vorname + " " + this.name;
	 }
	 //6. Implementieren Sie eine Methode 'vollname', die den vollen Namen (Vor- und Zuname) als string zur�ckgibt.
}
